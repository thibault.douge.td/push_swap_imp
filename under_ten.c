/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   under_ten.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 17:05:19 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/03 17:05:21 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	three_class(t_swp *swp)
{
	if (swp->tab_a[0] > swp->tab_a[1] && swp->tab_a[0] > swp->tab_a[2])
	{
		rotate_a(swp);
		if (swp->tab_a[0] > swp->tab_a[1])
			swap_a(swp);
		return ;
	}
	if (swp->tab_a[0] > swp->tab_a[1] && swp->tab_a[0] < swp->tab_a[2])
		swap_a(swp);
	if (swp->tab_a[0] < swp->tab_a[1] && swp->tab_a[0] > swp->tab_a[2])
		reverse_a(swp);
	if (swp->tab_a[0] < swp->tab_a[1] && swp->tab_a[1] > swp->tab_a[2])
	{
		rotate_a(swp);
		swap_a(swp);
		reverse_a(swp);
	}
	return ;
}

void	under_ten(t_swp *swp)
{
	int i;

	i = 1;
	while (i != swp->len_tab)
	{
		if (swp->len_a < 3)
		{
			three_class(swp);
			return ;
		}
		if (find_sens(i, swp) == 0)
			while (swp->tab_a[0] != i)
				rotate_a(swp);
		else
			while (swp->tab_a[0] != i)
				reverse_a(swp);
		i++;
		push_b(swp);
	}
}
