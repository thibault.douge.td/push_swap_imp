# Push Swap

### Consignes: 

Ce projet nous demande de trier des données sur une pile, avec un set d’instructions limité, en moins de coups possibles. Pour le réussir, je devais manipuler différents algorithmes de tri et choisir la ou les solutions la plus appropriée pour un classement optimisé des données. Au départ l'utilisateur envoie une pile de nombre via l'entrée standard que l'on nommera pile A, et nous devons la trier avec maximum deux piles (pile A, pile B).

Les différentes opérations disponibles:

-sa : swap a - intervertit les 2 premiers éléments au sommet de la pile a. Ne fait
rien s’il n’y en a qu’un ou aucun.

-sb : swap b - intervertit les 2 premiers éléments au sommet de la pile b. Ne fait
rien s’il n’y en a qu’un ou aucun.

-ss : sa et sb en même temps.

-pa : push a - prend le premier élément au sommet de b et le met sur a. Ne fait
rien si b est vide.

-pb : push b - prend le premier élément au sommet de a et le met sur b. Ne fait
rien si a est vide.

-ra : rotate a - décale d’une position vers le haut tous les élements de la pile a.
Le premier élément devient le dernier.

-rb : rotate b - décale d’une position vers le haut tous les élements de la pile b.
Le premier élément devient le dernier.

-rr : ra et rb en même temps.

-rra : reverse rotate a - décale d’une position vers le bas tous les élements de
la pile a. Le dernier élément devient le premier.

-rrb : reverse rotate b - décale d’une position vers le bas tous les élements de
la pile b. Le dernier élément devient le premier.

-rrr : rra et rrb en même temps.

### Contraintes: 

Un set d'instructions limités, une gestion d'erreur à mettre en place, il n'est pas possible d'avoir de doublon, tout autre chose qu'un nombre doit générer une erreur. De plus je devais aussi créer un checker qui vérifie que la pile est bien trié.

Résolution: Pour réussir ce projet j'ai découvert les différents algorithmes de tri, et je m'en suis inspiré pour pouvoir réaliser mon propre algorithme de tri, en rescpectant le set d'opérations autorisés. J'ai créé un index des nombres pour pouvoir faire des médianes. Je commence par faire un pré tri grace aux médianes et ensuite je fais un tri par séléction pour terminer mon tri.

Fonction autorisées :

◦ write 
◦ read 
◦ malloc 
◦ free 
◦ exit

### Utilisation du programme:

Utilisation: make; ./push_swap ARG1 ARG2 ARG3 ARG4 .... Ou chaque arg est un element entier de la liste a trier.