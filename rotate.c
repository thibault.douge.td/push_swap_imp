/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 17:05:09 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/03 17:05:11 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	rotate_a(t_swp *swp)
{
	int i;

	i = 0;
	if (swp->len_a > 0)
	{
		swp->len_a++;
		swp->tab_a[swp->len_a] = swp->tab_a[0];
		while (i != swp->len_a)
		{
			swp->tab_a[i] = swp->tab_a[i + 1];
			i++;
		}
		swp->len_a--;
		if (swp->verif == 0 && swp->check == 0)
			write(1, "ra\n", 3);
	}
}

void	rotate_b(t_swp *swp)
{
	int i;

	i = 0;
	if (swp->len_b > 0 && swp->len_b)
	{
		swp->len_b++;
		swp->tab_b[swp->len_b] = swp->tab_b[0];
		while (i != swp->len_b)
		{
			swp->tab_b[i] = swp->tab_b[i + 1];
			i++;
		}
		swp->len_b--;
		if (swp->verif == 0 && swp->check == 0)
			write(1, "rb\n", 3);
		swp->verif = 0;
	}
}

void	rotate_rotate(t_swp *swp)
{
	swp->verif = 1;
	if (swp->check == 0)
		write(1, "rr\n", 3);
	rotate_a(swp);
	rotate_b(swp);
}
