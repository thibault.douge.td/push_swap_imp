/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   recup_arg.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 17:04:49 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/14 02:24:15 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	chack(char **argv, int i)
{
	int j;
	int tmp;

	tmp = 0;
	j = 0;
	if (ft_strcmp(argv[i], "") == 0)
	{
		write(1, "Error\n", 6);
		exit(0);
	}
	while (argv[i][j])
	{
		if (ft_isdigit(argv[i][j]) == 1)
			tmp++;
		j++;
	}
	if (tmp == 0)
	{
		write(1, "Error\n", 6);
		exit(0);
	}
}

void	len_tab(t_swp *swp, char **argv)
{
	int		i;
	int		len;
	char	**recup;
	int		j;

	len = 0;
	i = 1;
	while (argv[i])
	{
		chack(argv, i);
		recup = ft_strsplit(argv[i], ' ');
		j = -1;
		while (recup[++j])
			len++;
		free_array(recup);
		i++;
	}
	swp->tab_a = (int *)malloc(sizeof(int) * (len + 1));
	ft_bzero((void *)swp->tab_a, len - 1);
	swp->tab_b = (int *)malloc(sizeof(int) * (len + 1));
	ft_bzero((void *)swp->tab_b, len - 1);
	swp->len_b = -1;
	swp->len_a = len - 1;
	swp->len_tab = len;
}

void	recup_clean(char ***recup, int *i)
{
	free_array(*recup);
	recup = NULL;
	*i = *i + 1;
}

void	recup_arg(t_swp *swp, char **argv)
{
	int			i;
	char		**recup;
	int			len;
	int			j;
	long int	verif_int;

	j = 0;
	i = 1;
	len = 0;
	verif_int = 0;
	len_tab(swp, argv);
	while (argv[i])
	{
		recup = ft_strsplit(argv[i], ' ');
		j = 0;
		is_valid(recup);
		while (recup[j])
		{
			verif_int = ft_atoi(recup[j]);
			is_valid_3(verif_int);
			swp->tab_a[len++] = ft_atoi(recup[j++]);
		}
		recup_clean(&recup, &i);
	}
	is_valid_2(swp);
}
