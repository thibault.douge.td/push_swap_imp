/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_valid.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 17:03:33 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/03 17:03:35 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	is_valid_3(long int i)
{
	if (i > 2147483647 || i < -2147483648)
	{
		write(1, "Error\n", 6);
		exit(0);
	}
}

void	is_valid_2(t_swp *swp)
{
	int i;
	int j;

	i = 0;
	j = 1;
	while (i != swp->len_a + 1)
	{
		while (j < swp->len_a + 1)
		{
			if (swp->tab_a[i] == swp->tab_a[j])
			{
				write(1, "Error\n", 6);
				exit(0);
			}
			j++;
		}
		i++;
		j = i + 1;
	}
}

void	is_valid(char **str)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (str[i])
	{
		while (str[i][j])
		{
			if ((ft_isdigit(str[i][j]) == 0 && str[i][j] != '-') ||
				(str[i][j] == '-' && ft_isdigit(str[i][j - 1]) == 1)
				|| (str[i][j] == '-' && str[i][j + 1] == '-'))
			{
				write(1, "Error\n", 6);
				exit(0);
			}
			j++;
		}
		j = 0;
		i++;
	}
}

void	check_line(char *line)
{
	if (line[0] == '\0')
		return ;
	else if (ft_strcmp(line, "sa") && ft_strcmp(line, "sb") &&
		ft_strcmp(line, "ss") && ft_strcmp(line, "ra") &&
		ft_strcmp(line, "rb") && ft_strcmp(line, "rr") &&
		ft_strcmp(line, "rra") && ft_strcmp(line, "rrb") &&
		ft_strcmp(line, "pb") && ft_strcmp(line, "pa") &&
		ft_strcmp(line, "rrr"))
	{
		write(1, "Error\n", 6);
		exit(0);
	}
}
