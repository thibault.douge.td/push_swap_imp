/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_middle.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 17:04:29 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/03 17:04:31 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	push_middle3(t_swp *swp, int *how, int *end, int *i)
{
	if (swp->len_b > 1 && find_sens_a(*how, *end, swp) == 0)
	{
		while (swp->tab_b[0] != *how && swp->tab_b[0] != *how - 1 &&
			swp->tab_b[0] != *end && swp->tab_b[0] != *end + 1 &&
			swp->len_b > 0)
		{
			if (*i == 1)
			{
				rotate_rotate(swp);
				*i = 0;
			}
			else
				rotate_b(swp);
		}
	}
	else
		while (swp->tab_b[0] != *how && swp->tab_b[0] != *how - 1 &&
			swp->tab_b[0] != *end && swp->tab_b[0] != *end + 1 &&
			swp->len_b > 0)
			reverse_b(swp);
	if (*i == 1)
	{
		rotate_a(swp);
		*i = 0;
	}
}

void	push_middle2(t_swp *swp, int *how, int *end, int *i)
{
	if (swp->tab_b[0] == *end || swp->tab_b[0] == *end + 1)
	{
		swp->verif3++;
		push_a(swp);
		check_a2(swp);
		*i = *i + 1;
	}
	if (swp->verif2 == 2)
	{
		*how -= 2;
		swp->verif2 = 0;
	}
	if (swp->verif3 == 2)
	{
		*end += 2;
		swp->verif3 = 0;
	}
}

void	push_middle(t_swp *swp)
{
	int how;
	int end;
	int i;

	i = 0;
	end = swp->mediane + (swp->mediane / 2) + (swp->mediane / 10);
	swp->verif2 = 0;
	swp->verif3 = 0;
	how = swp->mediane + (swp->mediane / 2) + (swp->mediane / 10) - 1;
	while (swp->len_b != -1)
	{
		if (swp->tab_b[0] == how || swp->tab_b[0] == how - 1 ||
			swp->tab_b[0] == end || swp->tab_b[0] == end + 1)
		{
			if (swp->tab_b[0] == how || swp->tab_b[0] == how - 1)
			{
				swp->verif2++;
				push_a(swp);
				check_a(swp);
			}
			push_middle2(swp, &how, &end, &i);
		}
		push_middle3(swp, &how, &end, &i);
	}
}
