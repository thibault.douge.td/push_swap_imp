/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 17:04:21 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/14 02:27:49 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		verif(t_swp *swp)
{
	int i;
	int j;

	i = 0;
	j = 1;
	while (swp->tab_a[i] < swp->tab_a[j])
	{
		if (i == swp->len_a - 1)
			return (0);
		i++;
		j++;
	}
	return (1);
}

void	vid_struct(t_swp *swp)
{
	free(swp->tab_a);
	free(swp->tab_b);
	free(swp);
}

t_swp	*ft_initalize(void)
{
	t_swp *swp;

	swp = malloc(sizeof(t_swp));
	swp->tab_a = NULL;
	swp->tab_b = NULL;
	swp->verif = 0;
	swp->check = 0;
	return (swp);
}

int		*push_swap(char **argv)
{
	t_swp	*swp;

	swp = ft_initalize();
	recup_arg(swp, argv);
	convert_tab(swp);
	calcul_mediane(swp);
	if (verif(swp) == 0)
	{
		vid_struct(swp);
		return (0);
	}
	if (swp->len_tab <= 13)
	{
		under_ten(swp);
		final_push(swp);
	}
	else
	{
		push_mediane(swp);
		push_middle(swp);
	}
	vid_struct(swp);
	return (0);
}

int		main(int argc, char **argv)
{
	if (argc < 2)
		return (0);
	else
		push_swap(argv);
	return (0);
}
