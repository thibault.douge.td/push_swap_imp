/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reverse.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 17:05:01 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/03 17:05:02 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	reverse_a(t_swp *swp)
{
	int i;

	i = 0;
	if (swp->len_a > 0)
	{
		swp->len_a++;
		i = swp->len_a;
		while (i != 0)
		{
			swp->tab_a[i] = swp->tab_a[i - 1];
			i--;
		}
		swp->tab_a[0] = swp->tab_a[swp->len_a];
		swp->len_a--;
		if (swp->verif == 0 && swp->check == 0)
			write(1, "rra\n", 4);
	}
}

void	reverse_b(t_swp *swp)
{
	int i;

	i = 0;
	if (swp->len_b > 0)
	{
		swp->len_b++;
		i = swp->len_b;
		while (i != 0)
		{
			swp->tab_b[i] = swp->tab_b[i - 1];
			i--;
		}
		swp->tab_b[0] = swp->tab_b[swp->len_b];
		swp->len_b--;
		if (swp->verif == 0 && swp->check == 0)
			write(1, "rrb\n", 4);
		swp->verif = 0;
	}
}

void	reverse_reverse(t_swp *swp)
{
	swp->verif = 1;
	reverse_a(swp);
	reverse_b(swp);
	if (swp->check == 0)
		write(1, "rrr\n", 4);
}
