/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo_1.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 17:02:28 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/03 17:02:31 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		find_little(t_swp *swp, int z)
{
	int so1;
	int so2;
	int vi;

	so1 = 0;
	so2 = 1;
	vi = 0;
	while (swp->tab_a[vi] > z)
	{
		so1++;
		vi++;
	}
	vi = swp->len_a;
	while (swp->tab_a[vi] > z)
	{
		so2++;
		vi--;
	}
	if (so1 > so2)
		return (1);
	return (0);
}

void	suite_for(t_swp *swp, int i, int tmp, int j)
{
	while (swp->len_a != -1)
	{
		while (j != -1)
		{
			while (i != 0)
			{
				if (swp->tab_a[0] <= tmp)
				{
					push_b(swp);
					i--;
				}
				else if (find_little(swp, tmp) == 0)
					while (swp->tab_a[0] > tmp)
						rotate_a(swp);
				else
					while (swp->tab_a[0] > tmp)
						reverse_a(swp);
			}
			j--;
			i = swp->len_tab / 13;
			tmp = swp->len_tab - (i * j);
		}
		push_b(swp);
	}
}

void	push_mediane(t_swp *swp)
{
	int i;
	int tmp;
	int j;

	j = 12;
	i = swp->len_tab / 13;
	tmp = swp->len_tab - (i * j);
	suite_for(swp, i, tmp, j);
}
