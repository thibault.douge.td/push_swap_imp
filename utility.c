/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utility.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 17:05:27 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/03 17:05:30 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		find_sens_a(int i, int end, t_swp *swp)
{
	int tmp;
	int so1;
	int so2;

	so1 = 0;
	so2 = 1;
	tmp = 0;
	while (swp->tab_b[tmp] != i && swp->tab_b[tmp] != i - 1 &&
		swp->tab_b[tmp] != end && swp->tab_b[tmp] != end + 1)
	{
		tmp++;
		so1++;
	}
	tmp = swp->len_b;
	while (swp->tab_b[tmp] != i && swp->tab_b[tmp] != i - 1 &&
		swp->tab_b[tmp] != end && swp->tab_b[tmp] != end + 1)
	{
		tmp--;
		so2++;
	}
	if (so1 < so2)
		return (0);
	return (1);
}

int		find_sens(int i, t_swp *swp)
{
	int tmp;
	int so1;
	int so2;

	so1 = 0;
	so2 = 1;
	tmp = 0;
	while (swp->tab_a[tmp] != i)
	{
		tmp++;
		so1++;
	}
	tmp = swp->len_a;
	while (swp->tab_a[tmp] != i)
	{
		tmp--;
		so2++;
	}
	if (so1 < so2)
		return (0);
	return (1);
}

void	convert_tab(t_swp *swp)
{
	int i;
	int *new;
	int j;
	int tmp;

	j = 0;
	tmp = 1;
	i = 0;
	new = (int *)malloc(sizeof(int) * (swp->len_tab + 1));
	while (j != swp->len_a + 1)
	{
		while (i != swp->len_a + 1)
		{
			if (swp->tab_a[j] > swp->tab_a[i])
				tmp++;
			i++;
		}
		new[j] = tmp;
		tmp = 1;
		j++;
		i = 0;
	}
	free(swp->tab_a);
	swp->tab_a = new;
}

void	calcul_mediane(t_swp *swp)
{
	int i;
	int j;

	j = 0;
	i = 0;
	swp->mediane = 0;
	while (i != swp->len_a + 1)
	{
		j += swp->tab_a[i];
		i++;
	}
	j = j / (swp->len_a + 1);
	swp->mediane = j;
}

int		verif_rest(int i, t_swp *swp)
{
	int j;

	j = 0;
	while (j < swp->len_b + 1)
	{
		if (swp->tab_b[j] > i)
			return (1);
		j++;
	}
	return (0);
}
