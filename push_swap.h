/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 17:04:39 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/14 02:11:28 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "libft/libft.h"
# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# define BUFF_SIZE 3

typedef struct	s_swp
{
	int *tab_a;
	int *tab_b;
	int len_a;
	int len_b;
	int	len_tab;
	int mediane;
	int verif;
	int verif2;
	int verif3;
	int check;
}				t_swp;

void			push_b(t_swp *swp);
void			push_a(t_swp *swp);
void			swap_b(t_swp *swp);
void			swap_a(t_swp *swp);
void			swap_swap(t_swp *swp);
void			rotate_a(t_swp *swp);
void			rotate_b(t_swp *swp);
void			rotate_rotate(t_swp *swp);
void			reverse_b(t_swp *swp);
void			reverse_a(t_swp *swp);
void			reverse_reverse(t_swp *swp);
void			push_middle(t_swp *swp);
void			final_push(t_swp *swp);
int				get_next_line(const int fd, char **line);
void			convert_tab(t_swp *swp);
int				find_sens_a(int i, int end, t_swp *swp);
void			calcul_mediane(t_swp *swp);
void			push_mediane(t_swp *swp);
void			push_mediane_mid(t_swp *swp);
int				verif_rest(int i, t_swp *swp);
void			final_push(t_swp *swp);
void			push_mediane(t_swp *swp);
void			check_a2(t_swp *swp);
void			check_a(t_swp *swp);
void			calcul_new_med(t_swp *swp);
void			calcul_new_med2(t_swp *swp);
void			is_valid(char **str);
void			is_valid_2(t_swp *swp);
void			len_tab(t_swp *swp, char **argv);
void			recup_arg(t_swp *swp, char **argv);
int				find_sens(int i, t_swp *swp);
void			under_ten(t_swp *swp);
void			is_valid_3(long int i);
void			check_line(char *line);

#endif
