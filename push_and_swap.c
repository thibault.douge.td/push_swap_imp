/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_and_swap.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 17:04:00 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/03 17:04:03 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	push_b(t_swp *swp)
{
	int i;
	int j;

	i = swp->len_b;
	j = 0;
	if (swp->len_a > -1)
	{
		swp->len_b++;
		j = swp->len_b;
		while (i != -1)
			swp->tab_b[j--] = swp->tab_b[i--];
		swp->tab_b[0] = swp->tab_a[0];
		j = 0;
		i = 1;
		while (j != swp->len_a)
			swp->tab_a[j++] = swp->tab_a[i++];
		swp->len_a--;
		if (swp->check == 0)
			write(1, "pb\n", 3);
	}
}

void	push_a(t_swp *swp)
{
	int i;
	int j;

	i = swp->len_a;
	j = 0;
	if (swp->len_b > -1)
	{
		swp->len_a++;
		j = swp->len_a;
		while (i != -1)
			swp->tab_a[j--] = swp->tab_a[i--];
		swp->tab_a[0] = swp->tab_b[0];
		j = 0;
		i = 1;
		while (j != swp->len_b)
			swp->tab_b[j++] = swp->tab_b[i++];
		swp->len_b--;
		if (swp->check == 0)
			write(1, "pa\n", 3);
	}
}

void	swap_b(t_swp *swp)
{
	int tmp;

	tmp = 0;
	if (swp->len_b > 0)
	{
		tmp = swp->tab_b[0];
		swp->tab_b[0] = swp->tab_b[1];
		swp->tab_b[1] = tmp;
		if (swp->verif == 0 && swp->check == 0)
			write(1, "sb\n", 3);
		swp->verif = 0;
	}
}

void	swap_a(t_swp *swp)
{
	int tmp;

	tmp = 0;
	if (swp->len_a > 0)
	{
		tmp = swp->tab_a[0];
		swp->tab_a[0] = swp->tab_a[1];
		swp->tab_a[1] = tmp;
		if (swp->verif == 0 && swp->check == 0)
			write(1, "sa\n", 3);
	}
}

void	swap_swap(t_swp *swp)
{
	swp->verif = 1;
	swap_a(swp);
	swap_b(swp);
	if (swp->check == 0)
		write(1, "ss\n", 3);
}
