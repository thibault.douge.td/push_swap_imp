NAME = push_swap

NAME2 = checker

SRC = algo_1.c \
	algo_mediane.c \
	check_valid.c \
	push_and_swap.c \
	push_med.c \
	push_middle.c \
	push_swap.c \
	recup_arg.c \
	reverse.c \
	rotate.c \
	under_ten.c \
	utility.c

SRC2 = algo_1.c \
	algo_mediane.c \
	check_valid.c \
	push_and_swap.c \
	push_med.c \
	push_middle.c \
	recup_arg.c \
	reverse.c \
	rotate.c \
	under_ten.c \
	utility.c \
	checker.c \
	get_next_line.c

OBJ = $(SRC:.c=.o)

OBJ2 = $(SRC2:.c=.o)

FLAGS	= -Wall -Wextra -Werror -g

LIB_A = -L./libft/ -lft

.PHONY: all libft clean fclean re

all: $(NAME) $(NAME2)

libft:
	make -C libft

$(NAME): $(OBJ) libft
	gcc $(FLAGS) $(OBJ) -o $(NAME) $(LIB_A)

$(NAME2): $(OBJ2) libft
	gcc $(FLAGS) $(OBJ2) -o $(NAME2) $(LIB_A)

%.o: %.c
	gcc $(FLAGS) -I ./ -I ./libft/ -c $?

clean:
	rm -f $(OBJ) $(OBJ2)
	make clean -C ./libft/

fclean: clean
	rm -f $(NAME) $(NAME2)
	make fclean -C ./libft/

re: fclean all