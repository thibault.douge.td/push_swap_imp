/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo_mediane.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 17:03:18 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/03 17:03:21 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	cut_calcul_new_med(t_swp *swp, int i, int j, int n)
{
	while (i != 0)
	{
		if (swp->tab_a[0] < j)
		{
			push_b(swp);
			i--;
		}
		else
		{
			rotate_a(swp);
			i--;
			n++;
		}
	}
	while (n != 0)
	{
		reverse_a(swp);
		push_b(swp);
		n--;
	}
}

void	calcul_new_med(t_swp *swp)
{
	int i;
	int j;
	int s;
	int n;

	n = 0;
	i = swp->len_a / 2;
	j = 0;
	s = i;
	while (i != 0)
	{
		i--;
		j += swp->tab_a[0];
	}
	j = j / s;
	i = s;
	cut_calcul_new_med(swp, i, j, n);
}

void	cut_calcul_new_med2(t_swp *swp, int i, int j, int n)
{
	while (i != 0)
	{
		if (swp->tab_a[0] < j)
		{
			push_b(swp);
			i--;
		}
		else
		{
			rotate_a(swp);
			i--;
			n++;
		}
	}
	while (n != 0)
	{
		reverse_a(swp);
		push_b(swp);
		n--;
	}
}

void	calcul_new_med2(t_swp *swp)
{
	int i;
	int j;
	int s;
	int n;

	n = 0;
	i = swp->len_a + 1;
	j = 0;
	s = i;
	while (i != -1)
	{
		i--;
		j += swp->tab_a[0];
	}
	j = j / s;
	i = s;
	cut_calcul_new_med2(swp, i, j, n);
}
