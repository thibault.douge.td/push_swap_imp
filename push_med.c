/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_med.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 17:04:10 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/03 17:04:11 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	final_push(t_swp *swp)
{
	int i;

	i = swp->len_b;
	while (i != -1)
	{
		push_a(swp);
		i--;
	}
}

void	check_a2(t_swp *swp)
{
	if (swp->len_a > 0)
	{
		if (swp->tab_a[0] < swp->tab_a[swp->len_a])
		{
			reverse_a(swp);
			swap_a(swp);
			rotate_a(swp);
		}
	}
}

void	check_a(t_swp *swp)
{
	if (swp->len_a > 0)
	{
		if (swp->tab_a[0] > swp->tab_a[1])
			swap_a(swp);
	}
}
