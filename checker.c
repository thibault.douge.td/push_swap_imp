/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 17:03:45 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/14 02:28:17 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	validate(t_swp *swp)
{
	int i;

	i = 0;
	if (swp->len_b != -1)
	{
		write(1, "KO\n", 3);
		return ;
	}
	while (i != swp->len_a)
	{
		if (swp->tab_a[i] > swp->tab_a[i + 1])
		{
			write(1, "KO\n", 3);
			return ;
		}
		i++;
	}
	write(1, "OK\n", 3);
}

t_swp	*ft_initalize(void)
{
	t_swp *swp;

	swp = malloc(sizeof(t_swp));
	swp->verif = 0;
	return (swp);
}

void	checker2(char *line, t_swp *swp)
{
	if (ft_strcmp(line, "ss") == 0)
		swap_swap(swp);
	else if (ft_strcmp(line, "ra") == 0)
		rotate_a(swp);
	else if (ft_strcmp(line, "rb") == 0)
		rotate_b(swp);
	else if (ft_strcmp(line, "rr") == 0)
		rotate_rotate(swp);
	else if (ft_strcmp(line, "rra") == 0)
		reverse_a(swp);
	else if (ft_strcmp(line, "rrb") == 0)
		reverse_b(swp);
	else if (ft_strcmp(line, "rrr") == 0)
		reverse_reverse(swp);
	else if (ft_strcmp(line, "pb") == 0)
		push_b(swp);
	else if (ft_strcmp(line, "pa") == 0)
		push_a(swp);
}

void	checker(char **argv)
{
	t_swp	*swp;
	char	*line;

	swp = ft_initalize();
	recup_arg(swp, argv);
	swp->check = 1;
	line = NULL;
	while (get_next_line(0, &line) == 1)
	{
		check_line(line);
		if (ft_strcmp(line, "sa") == 0)
			swap_a(swp);
		else if (ft_strcmp(line, "sb") == 0)
			swap_b(swp);
		checker2(line, swp);
		free(line);
		line = NULL;
	}
	free(line);
	swp->check = 0;
	validate(swp);
	free(swp->tab_a);
	free(swp->tab_b);
	free(swp);
}

int		main(int argc, char **argv)
{
	if (argc < 2)
		return (0);
	checker(argv);
	return (0);
}
